# Laboratorio 02: Gestión de ramas con Git Flow. 

## Primera Parte

Hacer grupos de 3 personas
Asignar roles, así:
- 2 Developers
- 1 Release manager

Tareas:

1. Crear proyecto y el nombre corresponde a un nombre que le den al equipo.
2. Crear rama Development a partir de la rama principal (Main/Master).
3. Validar que esten bloqueadas o que no permitan subir cambios directamente. (Push)
4. Validar que todos los miembros del equipo tengan acceso al proyecto.
5. Developer 1 crea rama ***feature_1*** y realizar:
   1. Crear archivo vacio de nombre ***integrantes.txt***
   2. Realizar comandos necesarios para subir la rama temporal a repositorio remoto.
6. Developer 2 clona rama feature_1
7. Developer 1 y 2 agregan sus nombres en el archivo ***integrantes.txt***
8. Developer 1 y 2 confirman cambios al repositorio remoto (Solucionar conflictos).
9. Developer 1 realizar Merge Request a la rama Dev.
10. Release Manager valida que esten los nombres de los developers y acepta el MR.
11. Release Manager genera rama Release.
12. Falta el nombre de un integrante del equipo (Release Manager), generar un bug en la rama release.
13. Se ponen de acuerdo cual developer lo resolvera, clona rama release.
14. Generar rama bug y agregar el nombre del release manager en el archivo ***integrantes.txt***
15. Confirmar la rama bug en repositorio remoto y hacer Merge Request a la rama release.
16. Release Manager acepta Merge Request si sus datos son correctos.
17. Release manager realiza Merge Request a rama Main.
18. Release manager genera Release y genera tag con la estretegía de versionamiento semantico, aumentando el digito de Cambio Mayor.

## Segunda parte

1. Realizar una mejora
   1. Como equipo nos auto-organizamos y generamos algun dato adicional en los datos suministrados en el archivo ***integrantes.txt***.
   2. Tener en cuenta:
      1. Realizar cambios en ramas temporales, prohibido subir cambios directamente a ramas de larga duración (Dev/Main)
      2. Contemplar flujo Git Flow.
      3. Al momento de subir el cambio a Main, validar cual digito de versionamiento semántico (X.Y.Z) se debe aumentar y generar el tag correspondiente.
2. Realizar un Feature nuevo.
   1. Auto-organizarse e incluir un nuevo archivo con algun texto a elección para subir.
   2. Tener en cuenta:
      1. Realizar cambios en ramas temporales, prohibido subir cambios directamente a ramas de larga duración (Dev/Main)
      2. Contemplar flujo Git Flow.
      3. Al momento de subir el cambio a Main, validar cual digito de versionamiento semántico (X.Y.Z) se debe aumentar y generar el tag correspondiente.
3. Solucionar un Hotfix.
   1. Validar algun cambio pequeño que se deba hacer en rama Main, simulando una incidencia productiva.
   2. Tener en cuenta:
      1. Realizar cambios en ramas temporales, prohibido subir cambios directamente a ramas de larga duración (Dev/Main)
      2. Contemplar flujo Git Flow.
      3. Al momento de subir el cambio a Main, validar cual digito de versionamiento semántico (X.Y.Z) se debe aumentar y generar el tag correspondiente.
