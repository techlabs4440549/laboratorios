# Laboratorio 03: Compilación

De acuerdo a los temas vistos en las sesiones de:

* **Plan** (*issue board, labels, milestones, release, tag*)
* **Code** (*Branch Strategies*)
* **Build** (*runners*, *pipelines*, *parallel jobs*, *CI/CD variables*, *CI/CD Components*)

Realizar las guientes tareas:

## Tareas

1. Crear un proyecto, para realizar la aplicación ***fizzbuzzapp***.
2. Crear issues y tareas asociadas a los [requerimientos](#requerimientos-fizzbuzzapp) de la aplicación.
3. Crear Milestones con las entregas a realizar.
4. Seleccionar estrategia de ramificación preferida.
5. Utilizar versionamiento semantico x.y.z.
6. Crear pipeline con *stage build*
7. Crear un proyecto de componentes.
8. Crear un componente que reciba alguna variable y utlizar dicho componente en el pipeline de *fizzbuzzapp*

## Requerimientos FizzBuzzApp.

Crear una app en *Java* para cubrir los siguientes requerimientos de negocio, que se encuentran organizados de acuerdo a sus prioridades, así:

#### MVP 1

1. Aplicación que reciba números enteros positivos y para aquellos números que sean multiplos de tres (3), imprimir la palabra ***fizz***, para aquellos que no sean multiplos de tres (3), se debera retornar el mismo número, ejemplo de salida:

```text
1
2  
Fizz  
4  
5
Fizz
7  
8
fizz
```