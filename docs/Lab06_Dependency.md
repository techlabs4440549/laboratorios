# Laboratorio 06: Instalación de dependencia

Laboratorio en el que practicaremos:

1. Configuración proyecto.
2. Verificación de permisos en repositorio de paquetes.
3. Pólitica de limpieza de paquetes.

## Tareas

1. Ir al proyecto FizzBuzz.
2. Configurar en el POM la dependencia, como lo indica la libreria creada.
3. Correr comando *mvn install*, para que se descargue la libreria en el proyecto.
4. Analizar que sucede en caso de que se genere un error.
5. Realizar una prueba unitaria que utilice el metodo de saludo que contiene la librería.
6. Realizar push del código.
7. Verificar el log de la ejecución del pipeline, buscando la descarga de la libreria relacionada.
