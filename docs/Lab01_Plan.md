# Laboratorio 01: Planeación y gestión del trabajo

En este laboratorio abordaremos los siguientes temas:

- Creación de [grupos y subgrupos](https://docs.gitlab.com/ee/user/group/).
- Creación de [proyectos](https://docs.gitlab.com/ee/user/project/).
- Creación de [Labels y Listas](https://docs.gitlab.com/ee/user/project/labels.html).
- Creación de [Milestones](https://docs.gitlab.com/ee/user/project/milestones/).
- Creación de [issues](https://docs.gitlab.com/ee/user/project/issues/)

## Tareas

1. Crear un grupo con el nombre ***CursoGitLab***
2. Crear un proyecto con el nombre ***HolaMundo***
3. Crear un label con el nombre ***En Progreso*** (A nivel de Grupo)
4. Agregar a ***Issue boards*** una lista con el label creado.
5. Crear un Milestone con el nombre ***Feature Hola Mundo***, para las fechas de incio y fin, tomar la fecha actual.
6. Crear un Issue con:
   1. Titulo ***Registrar Participante***
   2. Asociar a milestone creado ***Feature Hola Mundo***.
   3. Dentro de la issue, crear las siguiente tareas:
      1. Crear rama con nombre "HolaMundo".
      2. Crear archivo *Participantes.txt*
      3. Agregar nombre participante en el archivo creado.
      4. Realizar merge request a la rama main. (Aprobar)
      5. Crear Release y Tag con nombre ***V1.0.0***
      6. Cerrar issue y milestone.
7. Poner la issue en la lista de ***En Progreso*** y realizar las actividades creadas dentro de la issue ***Registrar Participante***.

Nota: Con cada commit o confirmación de cambios en el repositorio agregar un comentario asociando la issue correspondiente.
