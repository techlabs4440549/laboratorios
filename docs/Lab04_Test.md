# Laboratorio 04: Test

Laboratorio en el que practicaremos:

1. Análisis estático de código con Sonar.
2. Métricas y Quality Gate en Sonar.
3. Impacto en pipeline por Quality Gate.
4. [Dependency Check](https://owasp.org/www-project-dependency-check/) (Análisis de dependencias de terceros).
5. Impacto en pipeline por vulnerabilidades en dependency check.

## Tareas

1. Crear cuenta en [SonarCloud.io](https://sonarcloud.io/) asociada a la cuenta de gitlab.com.
2. Importar el grupo como *Organization* en *SonarCloud*
3. Crear proyecto de la app *FizzBuzz* en SonarCloud.io
4. Agregar al pipeline el job para análisis estático de código con Sonar Cloud (Seguir indicaciones de [SonarCloud.io](https://sonarcloud.io/)).
5. Agregar impacto al pipelines de acuerdo al *Quality Gate*.
6. De acuerdo a estrategia para abordar la deuda técnica explicada, solucionar algunos incidentes de acuerdo al impacto (Seguridad, Bug, Code Smell).
7. Desarrollorrar los requerimientos planteados. Realizar estrategia de versionamiento seleccionada.
8. Agregar análisis estático de dependencias de terceros con *Dependency Chek*.
9. Agregar impacto al pipeline, de acuerdo a las vulnerabilidades detectadas en *dependency check*.

### Documentación en el *readme.md* del proyecto

1. Indicar estrategia de versionamiento que se esta utilizando en el proyecto.
2. Subir imagen del primer release generado.
3. Agregar imagen con las métricas iniciales que arrojo *Sonar Cloud*, indicar cuales issues se van a corregir y luego imagen de sonar con las métrcias posteriores a los ajustes.
4. Docuementar que requerimientos se entregaron en cada release. 

### Requerimientos FizzBuzzApp

Agregar las siguientes features a la app:

#### MVP 2

1. Agregar a la aplicación funcionalidad que permita identificar aquellos números que sean multiplos de cinco (5), imprimir la palabra ***buzz***, para aquellos que no sean multiplos de cinco (5), se debera retornar el mismo número.
2. Asegurarse de no dañar la funcionalidad implementada en el MVP 1. ejemplo de salida:

```text
1
2  
Fizz  
4  
Buzz
Fizz
7  
8
Fizz
Buzz
```