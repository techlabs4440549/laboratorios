# Laboratorio 05: Package Registry

Laboratorio en el que practicaremos:

1. Configuración ambiente Package Registry.
2. Generación de libreria en package registry.

## Tareas

1. Crear un grupo para gestionar en este los proyectos de librerias. *(Nombre libre)*
2. Crear los subgrupos:
   1. *CodigoLibrerias*
   2. *Paquetes*
3. Crear un proyecto con nombre *MavenDependencies* vacio dentro del subgrupo *Paquetes*.
4. Crear dentro del subgrupo *CodigoLibrerias* un proyecto *simple-maven-dep*, asi:
   1. Importar proyecto <https://gitlab.com/techlabs4440549/tdd/simple-maven-dep.git>
5. Validar configuraciones de:
   1. archivo ci_settings.xml.

   ```xml
    <settings>
    <servers>
        <server>
        <id>gitlab-maven</id>
        <configuration>
            <httpHeaders>
            <property>
                <name>REPLACE_WITH_NAME</name>
                <value>REPLACE_WITH_TOKEN</value>
            </property>
            </httpHeaders>
        </configuration>
        </server>
    </servers>
    </settings>
    ```

    2. archivo pom.xml.

    ```xml
    <repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
    </repository>
    </repositories>
    <distributionManagement>
    <repository>
        <id>gitlab-maven</id>
        <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
    </repository>
    <snapshotRepository>
        <id>gitlab-maven</id>
        <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
    </snapshotRepository>
    </distributionManagement>
    ```
6. Crear pipeline y agregar el job:

```yaml
deploy:
  image: maven:3.6-jdk-11
  script:
    - 'mvn deploy -s ci_settings.xml'
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```